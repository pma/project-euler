CXX=g++

LIBDIR=./lib
OBJDIR=./obj
SRCDIR=./src

# Libeuler shared library variables
LIBEULERDIR=./libeuler
LIBEULERSRCDIR=$(LIBEULERDIR)/src
LIBEULERINCDIR=$(LIBEULERDIR)/include
LIBEULEROBJ=$(patsubst $(LIBEULERSRCDIR)/%.cpp, $(OBJDIR)/%.o, $(wildcard $(LIBEULERSRCDIR)/*.cpp))

# Library linker flags
LIBDIRS=$(LIBDIR)
LIBDIRFLAG=$(addprefix -L, $(LIBDIRS))
LIBS=euler
LIBFLAGS=$(addprefix -l, $(LIBS))

# Include flags
INCDIRS=$(LIBEULERINCDIR)
INCDIRFLAG=$(addprefix -I,$(INCDIRS))

# CXX flags
OTIMIZSEFLAGS=-O2
DEBUGFLAGS=-ggdb
OPTIONALFLAGS=
ifdef DEBUG
	OPTIONALFLAGS=$(DEBUGFLAGS)
else
	OPTIONALFLAGS=$(OTIMIZSEFLAGS)
endif
CXXFLAGS=$(OPTIONALFLAGS) -w -MMD -fPIC -L$(LIBDIR) $(INCDIRFLAG) -Wall -Werror -Wextra

# List executables
EXECUTABLES=$(basename $(notdir $(wildcard $(SRCDIR)/*.cpp)))

# Compile all problems
.PHONY: all
all: $(EXECUTABLES)

# Run all problems
.PHONY: run-all
run-all: $(addprefix run-,$(EXECUTABLES))

# Include generated dependencies 
DEPFILES=$(patsubst %.o, %.d, $(wildcard $(OBJDIR)/*.o))
-include $(DEPFILES)

# Make objects for libeuler 
$(OBJDIR)/%.o: $(LIBEULERSRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $(filter %.cpp, $^)  

# Link libeuler.so
$(LIBDIR)/libeuler.so: $(LIBEULEROBJ)
	$(CXX) $(CXXFLAGS) -shared -o $@ $(filter %.o, $^)

# Compile problem%
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $(filter %.cpp, $^)

# Link problem%
Problem%: $(OBJDIR)/Problem%.o $(LIBDIR)/libeuler.so
	$(CXX) $(CXXFLAGS) $(LIBFLAGS) -o $@ $(filter %.o, $^)

.PHONY: run-Problem%
run-Problem%: Problem%
	./$^

.PHONY: new-Problem%
new-Problem%: | $(SRCDIR)/ProblemTemplate.cpp.template
	cp -n $| $(SRCDIR)/$(patsubst new-%,%,$@).cpp

# Remove compiled objects, dependencies, libraries, and executables
.PHONY: clean
clean:
	rm -vf $(OBJDIR)/* $(LIBDIR)/* $(EXECUTABLES)

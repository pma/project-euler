#pragma once

#include <euler/fibonacci.h>
#include <euler/fraction.h>
#include <euler/numberstring.h>
#include <euler/palindrome.h>
#include <euler/prime.h>
#include <euler/probability.h>
#include <euler/pyramid.h>

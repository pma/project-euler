#pragma once

#include <cstdint>

namespace euler {
	uint64_t reverseInt(uint64_t number, uint64_t base = 10);
	bool isPalindrome(uint64_t number, uint64_t base = 10);
	uint64_t makePalindrome(uint64_t number, uint64_t base, bool odd_length);
	uint64_t makePalindromeBase2(uint64_t number, bool odd_length);
}

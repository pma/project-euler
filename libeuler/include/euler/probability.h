#pragma once

namespace euler {
	long binomial_coefficient( int n, int k );
}

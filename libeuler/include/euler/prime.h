#pragma once

#include <cstdint>
#include <set>
#include <vector>

namespace euler {
	std::set<uint64_t> find_primes(uint32_t count, uint32_t limit = 0);
	std::vector<uint64_t> prime_factor(uint64_t number);
	std::vector<uint64_t> factor(uint64_t number);
	std::vector<uint64_t> proper_divisors(uint64_t number);
	bool is_prime(uint64_t number);
}

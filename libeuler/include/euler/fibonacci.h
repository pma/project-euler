#pragma once

#include <vector>

namespace euler {
	std::vector<int> fibonacci_sequence(unsigned int length, int limit);
}

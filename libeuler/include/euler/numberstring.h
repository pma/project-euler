#pragma once

#include <string>

namespace euler {
	std::string number_to_string(int number);
}

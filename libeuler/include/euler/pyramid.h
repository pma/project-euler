#pragma once

#include <vector>
#include <iostream>
#include <fstream>

namespace euler {
	template <typename T> struct Node{
		Node(T value);
		const T value;
		std::vector<Node<T>*> parents;
		std::vector<Node<T>*> children;
		int max;
		bool onMaxSumPath;
	};

	template <typename T> class Pyramid{
		public:
			Pyramid();
			Pyramid(std::string pyramidString);
			T max_sum_path();
			std::istream& parse(std::istream& is);
			std::ostream& print(std::ostream& os) const;
			
		private:
			std::vector< Node<T> > nodes;
			Node<T>* rootNode;
			std::vector< std::vector< Node<T>* > > rows;
			bool valid;
			unsigned levels;
			T maxNodeValue;
			bool maxSumPathFound;
			T maxPathSum = 0;
	};

	template<typename t> std::istream& operator>> (std::istream& is, Pyramid<t>& pramid);
	template<typename t> std::ostream& operator<< (std::ostream& os, const Pyramid<t>& pramid);
}

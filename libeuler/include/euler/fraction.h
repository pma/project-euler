#pragma once

#include <cstdint>
#include <iostream>

namespace euler {
	class Fraction {
	public:
		int64_t numerator;
		int64_t denominator;

		Fraction( );
		Fraction( const int64_t& num, const int64_t& den );
		Fraction( const int64_t& integer );

		explicit operator double() const;
		explicit operator int64_t() const;

		int64_t remainder() const;
		Fraction remainderFraction() const;
		Fraction& simplify();
		Fraction& inverse();

		Fraction operator-() const;
		Fraction& operator+=(const Fraction& rhs);
		Fraction& operator-=(const Fraction& rhs);
		Fraction& operator*=(const Fraction& rhs);
		Fraction& operator/=(const Fraction& rhs);

		Fraction& operator+=(const int64_t& rhs);
		Fraction& operator-=(const int64_t& rhs);
		Fraction& operator*=(const int64_t& rhs);
		Fraction& operator/=(const int64_t& rhs);

	private:
		int64_t gcd(int64_t num, int64_t den);

	};

	std::ostream& operator<<(std::ostream& os, const Fraction& fraction);

	Fraction operator+(Fraction lhs, const Fraction& rhs) {
		return lhs += rhs;
	}

	Fraction operator-(Fraction lhs, const Fraction& rhs) {
		return lhs -= rhs;
	}

	Fraction operator*(Fraction lhs, const Fraction& rhs) {
		return lhs *= rhs;
	}

	Fraction operator/(Fraction lhs, const Fraction& rhs) {
		return lhs /= rhs;
	}

	Fraction operator+(Fraction lhs, const int64_t& rhs) {
		return lhs += rhs;
	}

	Fraction operator+(const int64_t& lhs, Fraction rhs) {
		return rhs += lhs;
	}

	Fraction operator-(Fraction lhs, const int64_t& rhs) {
		return lhs -= rhs;
	}

	Fraction operator-(const int64_t& lhs, const Fraction& rhs) {
		return (-rhs) += lhs;
	}

	Fraction operator*(Fraction lhs, const int64_t& rhs) {
		return lhs *= rhs;
	}

	Fraction operator*(const int64_t& lhs, Fraction& rhs ) {
		return rhs *= lhs;
	}

	Fraction operator/(Fraction lhs, const int64_t& rhs) {
		return lhs /= rhs;
	}

	Fraction operator/(const int64_t& lhs, Fraction& rhs ) {
		return rhs.inverse() *= lhs;
	}

	bool operator==(const Fraction& lhs, const Fraction& rhs ) {
		Fraction lhs_copy(lhs);
		Fraction rhs_copy(rhs);
		lhs_copy.simplify();
		rhs_copy.simplify();
		return (lhs_copy.numerator == rhs_copy.numerator) && (lhs_copy.denominator == rhs_copy.denominator);
	}
}

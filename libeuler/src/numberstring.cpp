#include <euler/numberstring.h>

#include <sstream>
#include <map>
#include <vector>

namespace euler {
	static std::map<int,std::string> numberWords = {
		{1, "one"},
		{2, "two"},
		{3, "three"},
		{4, "four"},
		{5, "five"},
		{6, "six"},
		{7, "seven"},
		{8, "eight"},
		{9, "nine"},
		{10, "ten"},
		{11, "eleven"},
		{12, "twelve"},
		{13, "thirteen"},
		{14, "fourteen"},
		{15, "fifteen"},
		{16, "sixteen"},
		{17, "seventeen"},
		{18, "eighteen"},
		{19, "nineteen"},
	};

	static std::map<int, std::string> numberWordsTens = {
		{20, "twenty"},
		{30, "thirty"},
		{40, "forty"},
		{50, "fifty"},
		{60, "sixty"},
		{70, "seventy"},
		{80, "eighty"},
		{90, "ninety"}
	};

	static std::map<int, std::string> numberWordsLarge = {
		{100, "hundred"},
		{1000, "thousand"},
		{1e6, "million"},
		{1e9, "billion"},
	};


	std::string number_to_string(int number) {
		std::stringstream ss;
		std::vector<std::string> numberFragments;

		// Print return "zero" for number == 1.
		if(number == 0 ) {
			return "zero";
		}

		// Print minus for negative numbers.
		if( number < 0 ) {
			ss << "minus ";
			number = -number;
		}
		// Add billions, millions, thousands, and hundreds.
		for( auto n : {1000000000, 1000000, 1000, 100} ) {
			if( number >= n ) {
				numberFragments.push_back(numberWords[number/n] + " " + numberWordsLarge[n]);
				number %= n;
			}
		}

		// Add twenty to ninety-nine or one to nineteen,
		if( number != 0 ) {
			number %= 100;
			if( number > 19 ) {
				int lastDigit = number%10;
				if( lastDigit == 0 ) {
					numberFragments.push_back(numberWordsTens[number - lastDigit]);
				} else {
					numberFragments.push_back(numberWordsTens[number - lastDigit] + "-" + numberWords[lastDigit]);
				}
			} else {
				numberFragments.push_back(numberWords[number]);
			}
		}

		// Add commas and "and".
		for( auto it = numberFragments.begin(); it != numberFragments.end(); ++it ) {
			ss << *it;
			if( numberFragments.end() - it > 2 ) {
				ss << ", ";
			} else if( numberFragments.end() - it > 1 ) {
				ss << " and ";
			}
		}

		return ss.str();
	}
}

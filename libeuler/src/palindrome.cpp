#include <euler/palindrome.h>

namespace euler {
	uint64_t reverseInt(uint64_t number, uint64_t base){
		uint64_t reversed = 0;

		if( base == 2 ) {
			while( number > 0 ) {
				reversed = (reversed << 1) + (number & 0x1);
				number >>= 1;
			}
		} else {
			while( number > 0 ) {
				reversed = reversed*base + number%base;
				number /= base;
			}
		}
		
		return reversed;
	}

	bool isPalindrome(uint64_t number, uint64_t base) {
		return number == reverseInt(number, base);
	}

	uint64_t makePalindrome(uint64_t number, uint64_t base, bool odd_length) {
		if( base == 2 ) {
			return makePalindromeBase2(number,odd_length);
		} else {
			uint64_t palindrome = number;
			if( odd_length ) {
				number /= base;
			}
			while( number > 0 ) {
				palindrome = palindrome*base + number%base;
				number /= base;
			}
			return palindrome;
		}
	}

	uint64_t makePalindromeBase2(uint64_t number, bool odd_length) {
		uint64_t palindrome = number;
		if( odd_length ) {
			number >>= 1;
		}
		while( number > 0 ) {
			palindrome = (palindrome << 1) + (number & 0x1);
			number >>= 1;
		}
		return palindrome;
	}
}

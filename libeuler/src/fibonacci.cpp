#include <euler/fibonacci.h>

namespace euler {
	std::vector<int> fibonacci_sequence(unsigned int length, int limit) {
		std::vector<int> numbers;

		while( numbers.size() < length || length == 0 ){
			if( numbers.size() < 2 ) {
				numbers.push_back(1);
			} else {
				numbers.push_back(numbers.rbegin()[0] + numbers.rbegin()[1]);
			}
			if( numbers.back() > limit ) {
				numbers.pop_back();
				break;
			}
		}

		return numbers;
	}
}

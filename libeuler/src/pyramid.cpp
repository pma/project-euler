#include <euler/pyramid.h>

#include <iomanip>
#include <sstream>
#include <cmath>

namespace euler {
	template<typename T> Node<T>::Node(T value)
		: value(value)
		, max(0)
		, onMaxSumPath(false)
	{}

	template<typename T> Pyramid<T>::Pyramid()
		: rootNode(NULL)
		, valid(false)
		, maxNodeValue(0)
		, maxSumPathFound(false)
	{}

	template<typename T> Pyramid<T>::Pyramid(std::string pyramidString)
	{
		Pyramid();
		std::stringstream ss(pyramidString);
		parse(ss);
	}

	template<typename T> T Pyramid<T>::max_sum_path() {
		if( !valid ) {
			return 0;
		}

		if( maxSumPathFound ) {
			return maxPathSum;
		}

		for( auto it = rows.rbegin(); it != rows.rend(); ++it ) {
			auto row = *it;
			for( auto node : row ) {
				if( node->children.empty() ) {
					node->max = node->value;
				}
				for( auto parentNode : node->parents ) {
					auto testSum = node->max + parentNode->value;
					if( testSum > parentNode->max ) {
						 parentNode->max = testSum;
					}
				}
			}
		}

		maxPathSum = rootNode->max;

		Node<T>* node = rootNode;
		while( node != NULL ) {
			node->onMaxSumPath = true;
			T max = 0;
			Node<T>* nextNode = NULL;	
			for( auto childNode : node->children ) {
				if( childNode->max > max ) {
					max = childNode->max;
					nextNode = childNode;
				}
			}
			node = nextNode;
		}

		return maxPathSum;
	}

	template<typename T> std::istream& Pyramid<T>::parse(std::istream& is) {
		rootNode = NULL;
		nodes.clear();
		rows.clear();
		valid = true;
		maxNodeValue = 0;
		levels = 0;
		maxSumPathFound = false;

		Pyramid();

		T value;
		std::string line;
		while( std::getline(is, line, '\n') ) {
			std::stringstream ss(line);
			std::string word;
			unsigned elements = 0;
			levels++;
			while( ss >> value ) {
				nodes.push_back(value);
				elements++;
			}
			if( elements != levels ) {
				valid = false;
				break;
			}
		}

		if( nodes.size() < 1 ) {
			valid = false;
		}

		if( valid ) {
			rootNode = &nodes.front();
			unsigned levelStart = 0;
			for( unsigned level = 0; level < levels; level++ ) {
				levelStart += level;
				std::vector<Node<T>*> row;
				for( unsigned element = 0; element <= level; element++ ) {
					auto node = &nodes.at(levelStart+element);
					if( level+1 != levels ) {
						node->children.push_back(&nodes.at(levelStart+element+level+1));
						node->children.push_back(&nodes.at(levelStart+element+level+2));
						for( auto childNode : node->children ) {
							childNode->parents.push_back(node);
						}
					}
					row.push_back(node);
				}
				rows.push_back(row);
			}
		}

		for( auto node : nodes ) {
			if( node.value > maxNodeValue ) {
				maxNodeValue = node.value;
			}
		}

		return is;
	}

	template<typename T> std::ostream& Pyramid<T>::print(std::ostream& os) const {
		if( !valid ) {
			os << "Invalid pyramid data structure!" << std::endl;
		} else {
			unsigned digits = std::ceil(std::log10(maxNodeValue));
			std::string separator(digits, ' ');

			for( auto row : rows ) {
				std::cout << std::string(digits*(levels-row.size()), ' ');
				for( auto node : row ) {
					if( node->onMaxSumPath ) {
						std::cout << "\033[0;31m";
					}
					std::cout << std::setfill('0') << std::setw(digits)
					          << node->value << separator;
					std::cout << "\033[0m";
				}
				std::cout << std::endl;
			}
		}

		return os;
	}

	template<typename t> std::istream& operator>> (std::istream& is, Pyramid<t>& pyramid) {
		return pyramid.parse(is);
	}

	template<typename t> std::ostream& operator<< (std::ostream& os, const Pyramid<t>& pyramid) {
		return pyramid.print(os);
	}

	template class Pyramid<int>;
	template std::istream& operator>> (std::istream& is, Pyramid<int>& pyramid);
	template std::ostream& operator<< (std::ostream& os, const Pyramid<int>& pyramid);

	template class Pyramid<long>;
	template std::istream& operator>> (std::istream& is, Pyramid<long>& pyramid);
	template std::ostream& operator<< (std::ostream& os, const Pyramid<long>& pyramid);
}

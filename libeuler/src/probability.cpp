#include <euler/probability.h>

namespace euler {
	long binomial_coefficient( int n, int k ) {
		long b = 1;
		for( int i = 1; i <= k; i++ ) {
			b = b*(n + 1 - i)/i;
		}
		return b;
	}
}

#include <euler/prime.h>

#include <algorithm>
#include <cmath>
#include <iterator>

namespace euler{
	std::set<uint64_t> find_primes(uint32_t limit, uint32_t count) {
		std::set<uint64_t> primes;
		primes.insert(2);

		// Initialize sieve
		std::vector<bool> sieve;
		sieve.resize((limit-1)/2, false);

		// Run through all elements in the sieve
		auto markEnd = sieve.begin() + (floor(sqrt(limit))-1)/2 + 2;
		for( auto it = sieve.begin(); it != sieve.end(); ++it ) {
			// If an element is false it is prime.
			if( *it == false ) {
				// Store prime
				uint64_t prime = 2*(std::distance(sieve.begin(),it)+1) + 1;
				primes.insert(prime);

				if( primes.size() == count ) {
					break;
				}

				// Do not try to mark when prime is larger than the square root 
				// of the limit as these will already have been marked.
				if( it < markEnd ) {
					// Mark all elements in sieve that are multiples of the
					// prime as not prime.
					for( auto jt = it+prime; jt < sieve.end(); jt += prime ) {
						(*jt) = true;
					}
				}
			}
		}

		return primes;
	}

	std::vector<uint64_t> prime_factor(uint64_t number) {
		std::vector<uint64_t> factors;

		// Only factorise number larger than 1;
		if( number > 1 ) {
			uint64_t prime = 2;

			while( prime*prime <= number ) {
				if( number % prime == 0 ) {
					do {
						number /= prime;
						factors.push_back(prime);
					} while( number%prime == 0 );
				} else {
					++prime;
				}
			}
			if( number != 1 ) {
				factors.push_back(number);
			}
		}

		return factors;
	}

	std::vector<uint64_t> factor(uint64_t number) {
		std::vector<uint64_t> factors;
		if( number != 0 ) {
			auto primeFactors = prime_factor(number);
			factors.reserve( primeFactors.size() + 1 );
			factors.push_back(1);
			factors.insert( factors.end(), primeFactors.begin(), primeFactors.end() );
		}
		return factors;
	}

	std::vector<uint64_t> proper_divisors(uint64_t number) {
		auto factors = prime_factor(number);
		std::vector<uint64_t> divisors;
		divisors.push_back(1);

		for( auto factor : factors ) {
			std::vector<uint64_t> newDivisors;
			for( auto divisor : divisors ) {
				newDivisors.push_back(divisor*factor);
				if( std::find(divisors.begin(),divisors.end(),newDivisors.back()) != divisors.end() ) {
					newDivisors.pop_back();	
				}
			}
			divisors.insert(divisors.end(),newDivisors.begin(),newDivisors.end());
		}
		divisors.pop_back();
		return divisors;
	}

	bool is_prime(uint64_t number) {
		// All primes are larger then 1
		if( number > 1 ) {
			// Prime_factor the number
			auto factors = prime_factor(number);
			// If the number only has one prime factor the number is a prime
			if( factors.size() == 1 ) {
				return true;
			}
		}
		return false;
	}
}

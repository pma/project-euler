#include <euler/fraction.h>

namespace euler{
	Fraction::Fraction( ) 
		: numerator(1)
		, denominator(1)
	{}

	Fraction::Fraction( const int64_t& num, const int64_t& den )
		: numerator(num)
		, denominator(den)
	{}

	Fraction::Fraction( const int64_t& integer )
		: numerator(integer)
		, denominator(1)
	{}

	Fraction::operator double() const {
		return (double) numerator/denominator;
	}

	Fraction::operator int64_t() const {
		return numerator/denominator;
	}

	int64_t Fraction::remainder() const {
		return numerator%denominator;
	}

	Fraction Fraction::remainderFraction() const {
		return Fraction(numerator%denominator,denominator);
	}

	Fraction& Fraction::simplify() {
		int64_t divisor = gcd(numerator,denominator);
		if( denominator < 0 ) {
			divisor *= -1;
		}
		numerator /= divisor;
		denominator /= divisor;
		return *this;
	}

	Fraction& Fraction::inverse() {
		std::swap(numerator,denominator);
		return *this;
	}

	Fraction Fraction::operator-() const {
		return Fraction(-numerator, denominator);
	}
	
	Fraction& Fraction::operator+=(const Fraction& rhs) {
		if( rhs.denominator == denominator ) {
			numerator += rhs.denominator;
		} else {
			numerator *= rhs.denominator;	
			numerator += rhs.numerator*denominator;	
			denominator *= rhs.denominator;
		}
		return *this;
	}

	Fraction& Fraction::operator-=(const Fraction& rhs) {
		if( rhs.denominator == denominator ) {
			numerator -= rhs.denominator;
		} else {
			numerator *= rhs.denominator;	
			numerator -= rhs.numerator*denominator;	
			denominator *= rhs.denominator;
		}
		return *this;
	}

	Fraction& Fraction::operator*=(const Fraction& rhs) {
		int64_t div1 = gcd(numerator,rhs.denominator);
		int64_t div2 = gcd(rhs.numerator,denominator);
		numerator /= div1;
		denominator /= div2;
		numerator *= rhs.numerator/div2;
		denominator *= rhs.denominator/div1;
		simplify();
		return *this;
	}

	Fraction& Fraction::operator/=(const Fraction& rhs) {
		inverse() *= rhs;
		inverse();
		return *this;
	}

	Fraction& Fraction::operator+=(const int64_t& rhs) {
		numerator += denominator*rhs;
		return *this;
	}
	
	Fraction& Fraction::operator-=(const int64_t& rhs) {
		numerator -= denominator*rhs;
		return *this;
	}

	Fraction& Fraction::operator*=(const int64_t& rhs) {
		numerator *= rhs;
		return *this;
	}

	Fraction& Fraction::operator/=(const int64_t& rhs) {
		denominator *= rhs;
		return *this;
	}

	int64_t Fraction::gcd(int64_t num, int64_t den) {
		num = std::abs( num );
		den = std::abs( den );
		if( num%den == 0 ) {
			return den;
		} else if( den%num == 0 ) {
			return num;
		}

		int64_t commonDivisor = 1;
		int64_t divisor = 2;
		while( true ) {
			if( num%divisor == 0 && den%divisor == 0 ) {
				commonDivisor *= divisor;
				num /= divisor;
				den /= divisor;
				if( num == 0 || den == 0 ) {
					break;
				}
			} else if( divisor*commonDivisor>std::min(num,den) ) {
				break;
			} else {
				++divisor;
			}
		}
		return commonDivisor;
	}

	std::ostream& operator<<(std::ostream& os, const Fraction& fraction) {
		if( fraction.denominator == 1 ) {
			os << fraction.numerator;
		} else {
			os << fraction.numerator << "/" << fraction.denominator;
		}
		return os;
	}

}

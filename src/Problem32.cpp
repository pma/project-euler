#include <algorithm>
#include <cstdint>
#include <iostream>
#include <vector>

#include <euler.h>

int main(void) {
	std::vector<uint_fast32_t> numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
	uint_fast32_t sum_of_pandigital_products = 0;

	do {
		uint_fast32_t product = 0;
		for( uintptr_t i = 0; i < 4; ++i ) {
			product = product*10 + numbers.at(i);
		}

		for( uintptr_t i = 1; i <= 2; ++i ) {
			uint_fast32_t multiplican = 0;
			for( uintptr_t j = 4; j < 4+i; ++j ) {
				multiplican = multiplican*10 + numbers.at(j);
			}

			uint_fast32_t multiplier = 0;
			for( uintptr_t j = 4 + i; j < numbers.size(); ++j ) {
				multiplier = multiplier*10 + numbers.at(j);
			}

			if( multiplican*multiplier == product ) {
				sum_of_pandigital_products += product;
				std::cout << multiplican << " x " << multiplier << " = " << product << std::endl;
				std::sort( numbers.begin()+4, numbers.end(), std::greater<uint_fast32_t>());
				break;
			}
		}
	} while( std::next_permutation( numbers.begin(), numbers.end() ) );

	std::cout << sum_of_pandigital_products << std::endl;
	return 0;
}

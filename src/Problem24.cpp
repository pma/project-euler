#include <algorithm>
#include <iostream>

#include <euler.h>

int main(void) {
	uint64_t N = 1000000;
	std::string number("0123456789");

	for( uintptr_t i = 1; i < N ; ++i ) {
		auto first = number.end() - 2;
		while( first != number.begin()-1 ) {
			if( *first < *(first+1) ) {
				break;
			} else {
				--first;
			}
		}

		if( first == number.begin()-1 ) {
			break;
		} else {
			auto ceil = first + 1;
			for( auto it = ceil; it != number.end(); ++it ) {
				if( (*it > *first) && (*it < *ceil) ) {
					ceil = it;
				}
			}

			std::swap(*first,*ceil);
			std::reverse(first+1,number.end());
		}
	}

	std::cout << number << std::endl;
	return 0;
}

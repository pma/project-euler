#include <iostream>

int main(void) {
	long int number = 600851475143;
	long int factor = 2;

	while( factor != number ) {
		if( number%factor == 0 ) {
			number /= factor;
		} else {
			factor ++;
		}
	}
	
	std::cout << factor << std::endl;
	
	return 0;
}

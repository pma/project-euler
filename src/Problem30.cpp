#include <cmath>
#include <iostream>
#include <numeric>

#include <euler.h>

int main(void) {
	const uint64_t power = 5;

	std::vector<uint64_t> powered_digits;
	for( size_t i = 0; i < 10; ++i ) {
		powered_digits.push_back(std::pow(i,power));
	}

	uint64_t max_number = powered_digits.at(9);
	uint64_t number = 9;
	while( max_number >= number ) {
		max_number += powered_digits.at(9);
		number = number*10 + 9;
	}

	std::cout << max_number << std::endl;

	std::vector<uint64_t> numbers;
	for( size_t i = 2; i < max_number ; ++i ) {
		uint64_t number = i;
		uint64_t sum = 0;
		do {
			sum += powered_digits.at(number%10);
			number /= 10;
		} while ( number > 0 && sum <= i);
		if( i == sum ) {
			numbers.push_back(sum);
		}
	}

	for( auto number : numbers ) {
		std::cout << number << std::endl;
	}

	std::cout << "Sum = " << std::accumulate(numbers.begin(), numbers.end(),0) << std::endl;

	return 0;
}

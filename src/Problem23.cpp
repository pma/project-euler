#include <algorithm>
#include <numeric>
#include <cstdint>
#include <iostream>
#include <set>

#include <euler.h>

int main(void) {
	uint64_t N = 28123;

	std::vector<uint64_t> abundantNumbers;

	for( uintptr_t number = 1; number < N; ++number ) {
		auto divisors = euler::proper_divisors(number);
		if( number < std::accumulate(divisors.begin(),divisors.end(),0) ) {
			abundantNumbers.push_back(number);
		}
	}

	std::vector<bool> abundantCombination(N,false);

	for( auto it = abundantNumbers.begin(); it != abundantNumbers.end(); ++it ) {
		for( auto jt = it; jt != abundantNumbers.end(); ++jt ) {
			uint64_t combination = *it + *jt;
			if( combination < N ) {
				abundantCombination.at(combination) = true;
			}
		}
	}

	uint64_t sum = 0;
	for( uintptr_t number = 1; number < abundantCombination.size(); ++number ) {
		if( !abundantCombination.at(number) ) {
			sum += number;
		}
	}
	
	std::cout << sum << std::endl;

	return 0;
}

#include <iostream>
#include <cmath>

#include <euler.h>

int main(void) {
	unsigned long a, b, c;

	int sum = 1000;

	for( a = 1; a <= (sum - 3)/3  ; a++ ) {
		for( b = a+1; b <= (sum-1-a)/2; b++ ) {
			c = sum - (a+b);

			if( (a*a + b*b) == (c*c) ) {
				std::cout << "a=" << a << ", b=" << b << ", c=" << c << std::endl;
				std::cout << "a*b*c=" << a*b*c << std::endl;
				return 0;
			}
		}
	}
	return -1;
}

#include <iostream>
#include <numeric>
#include <sstream>

#include <euler.h>

int main(void) {
	const uint64_t upper_bound = 11;
	std::vector<uint64_t> truncatable_primes;
	
	auto primes = euler::find_primes(1000000,0);
	auto pit = primes.begin();

	while( *pit < 10 ) {
		++pit;
	}

	while( truncatable_primes.size() != upper_bound && pit != primes.end() ) {
		bool is_truncatable = true;
		uint64_t mask = 10;
		while( *pit/mask != 0 ) {
			if( primes.count(*pit%mask) == 0 || primes.count(*pit/mask) == 0 ) {
				is_truncatable = false;
			}
			mask *= 10;
		}

		if( is_truncatable ) {
			truncatable_primes.push_back(*pit);
		}
		++pit;
	}

	for( auto prime : truncatable_primes ) {
		std::cout << prime << std::endl;
	}

	std::cout << "Sum of all truncatable primes: " << std::accumulate(truncatable_primes.begin(), truncatable_primes.end(), 0) << std::endl;
	return 0;
}

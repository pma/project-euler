#include <iostream>
#include <cmath>
#include <vector>

int main(void) {
	int k = 20;

	// Find prime numbers smaller than K
	std::vector<int> primes;
	for( int number = 2; number <= k; number++ ) {
		int i = 2;
		while( number%i ) {
			i++;
		};
		if( number == i ) {
			primes.push_back(number);
		}
	}

	int number = 1;
	int limit = floor(sqrt(k));

	// For each prime find the largest k satisfying prime^c < k
	for( auto prime : primes ) {
		int coefficient = 1;
		// Only try coefficient other than 1 where prime^2 < k
		if( prime <= limit ) {
			coefficient = floor( log(k) / log(prime) );
		}
		number *= pow(prime,coefficient);
	}

	std::cout << number << std::endl;
	return 0;
}

#include <iostream>

#include <euler.h>

int main(void) {
	const unsigned N = 1001;

	uint64_t diagonalSum = 1;

	for( unsigned dim = 3;  dim <= N; dim += 2 ) {
		diagonalSum += 4*dim*dim - 6*(dim) + 6;
	}

	std::cout << diagonalSum << std::endl;
	return 0;
}

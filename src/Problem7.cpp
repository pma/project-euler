#include <iostream>
#include <euler/prime.h>

int main(void) {
	auto primes = euler::find_primes(2<<16,10001);
	std::cout << *primes.rbegin() << std::endl;
	return 0;
}

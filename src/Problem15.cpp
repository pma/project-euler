#include <iostream>

#include <euler/probability.h>

int main(void) {
	int gridSize = 20;
	long routes = euler::binomial_coefficient( 2*gridSize, gridSize );

	std::cout << routes << std::endl;
	return 0;
}

#include <algorithm>
#include <iostream>

#include <euler.h>

int main(void) {
	unsigned N = 1000;
	unsigned dLongest = 0;
	unsigned longest = 0;

	for( auto d : euler::find_primes(N) ) {
		std::vector<unsigned> remainders;
		remainders.push_back(1);
		while( true ) {
			unsigned remainder = (remainders.back()*10)%d; 
			if( remainder == 0 ) {
				break;
			}
			auto prev = std::find( remainders.begin(), remainders.end(), remainder );
			if( prev != remainders.end() ) {
				unsigned length = std::distance( prev, remainders.end() );
				if( length > longest ) {
					longest = length;
					dLongest = d;
				}
				break;
			}
			remainders.push_back(remainder);
		}
	}

	std::cout << "d=" << dLongest << " gives " << longest << std::endl;
	return 0;
}

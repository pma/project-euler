#include <iostream>
#include <numeric>

#include <euler.h>

bool is1To9Pandigital(uint64_t number) {
	// Truncate to the 9 most significant digits.
	while( number > 1000000000 ) {
		number /= 10;
	}

	// Disqualify numbers smaller than the smallest pandigital number or larger
	// than the biggest.
	if( number < 123456789 || 987654321 < number ) {
		return false;
	}

	std::vector<uint_fast8_t> exists(10, false);
	while( number != 0 ) {
		exists.at(number%10) = 1;
		number /= 10;
	}

	if( std::accumulate( exists.begin()+1, exists.end(), 0 ) == 9 ) {
		return true;
	} else {
		return false;
	}
}

int main(void) {
	const uint64_t largest_pandigital = 987654321; // Largest pandigital 9-digit number
	const uint64_t smallest_pandigital = 123456789; // Largest pandigital 9-digit number

	uint64_t upper_bound = 9999; // cat( 9999*1=9999, 9999*2=19998 ) = 999919998 
	while( upper_bound*100000 + upper_bound*2 >= largest_pandigital && upper_bound >= 5000 ) {
		--upper_bound;
	}
	std::cout << "Upper bound for search: " << upper_bound << std::endl;

	uint64_t largest_concatenated_pandigital = 0;
	for( uint64_t i = upper_bound; i > 1 && largest_concatenated_pandigital == 0; --i ) {
		uint64_t concatenated_number = i;
		uint64_t n = 2;
		for( uint64_t j = n; ; ++j ) {
			n = j;
			uint64_t number = i*j;
			uint64_t magnitude = 10;
			while( magnitude < number ) {
				magnitude *= 10;
			}
			concatenated_number = concatenated_number*magnitude + number;
			if( smallest_pandigital <= concatenated_number ) {
				break;
			}
		}
		if( concatenated_number <= largest_pandigital) {
			if( is1To9Pandigital(concatenated_number) ) {
				largest_concatenated_pandigital = concatenated_number;
				for( uint64_t j = 1; j <= n; ++j ) {
					std::cout << i << "x" << j << " = " << i*j << std::endl; 
				}
			}
		}
	}

	std::cout << largest_concatenated_pandigital << std::endl;
	return 0;
}

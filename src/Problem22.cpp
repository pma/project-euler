#include <fstream>
#include <iostream>
#include <list>

#include <euler.h>

bool operator<(const std::string& lhs, const std::string& rhs) {
	auto lit = lhs.begin();
	auto rit = rhs.begin();
	while( (lit != lhs.end()) && (rit != rhs.end()) ) {
		if( *lit < *rit ) {
			return true;
		} else if( *lit > *rit ) {
			return false;
		} else {
			++lit;
			++rit;
		}
	}
	if( lhs.size() < rhs.size() ) {
		return true;
	} else { 
		return false;
	}
}

template <typename T> std::vector<T> sort( std::vector<T> list ) {
	bool sorted;
	do {
		sorted = true;
		for( auto it = list.begin(); it+1 != list.end(); ++it ) {
			for( auto jt = it; (*(jt+1) < *jt) && (jt+1 != list.end()); ++jt ) {
				auto temp = *jt;
				*jt = *(jt+1);
				*(jt+1) = temp;
				sorted = false;
			}
		}
	} while( !sorted );
	
	return list;
}

template <typename T> std::list<T> merge_sort( const std::list<T>& list ) {
	if( list.size() == 1 ) {
		return list;
	} else {
		auto split = list.begin();
		std::advance(split,list.size()/2);
		auto list1 = merge_sort( std::list<T>(list.begin(), split) );
		auto list2 = merge_sort( std::list<T>(split, list.end()) );
		std::list<T> list3;

		auto it1 = list1.begin();
		auto it2 = list2.begin();

		while( !list1.empty() && !list2.empty() ) {
			if( list1.front() < list2.front() ) {
				list3.splice(list3.end(), list1, list1.begin() );
			} else {
				list3.splice(list3.end(), list2, list2.begin() );
			}
		}
		if( !list1.empty() ) {
			list3.merge(list1);
		} else if( !list2.empty() ) {
			list3.merge(list2);
		}
		return list3;
	}
}

unsigned name_score(const std::string& name) {
	unsigned nameScore = 0;
	for( char c : name ) {
		nameScore += c - 64;
	}
	return nameScore;
}

int main(void) {
	std::ifstream nameFile;
	nameFile.open("./data/p022_names.txt");
	if( !nameFile.is_open() ) {
		std::cout << "Failed to open file!" << std::endl;
		return 1;
	}

	std::list<std::string> names;
	std::string name;

	while( std::getline(nameFile,name,',') ) {
		auto begin = name.find_first_not_of('"');
		auto end = name.find_last_of('"');
		name = name.substr(begin,end-begin);
		names.push_back(name);
	}

	names = merge_sort(names);
	unsigned nameNumber = 0;
	unsigned nameScoreSum = 0;

	for( auto name : names ) {
		nameNumber++;
		auto nameScore = name_score(name);
		nameScoreSum += nameNumber*nameScore;
	}

	std::cout << nameScoreSum << std::endl;
	return 0;
}

#include <iostream>

#include <euler.h>

int main(void) {
	unsigned int charCount = 0;

	for( int number = 1; number <= 1000; number++ ) {
		std::string numberString = euler::number_to_string(number);
		for( auto c : numberString ) {
			if( c != ' ' && c != ',' && c != '-' ) {
				charCount++;
			}
		}
		
		std::cout << number << " " << numberString << std::endl;
	}

	std::cout << charCount << std::endl;
	return 0;
}

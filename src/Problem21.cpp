#include <cmath>
#include <iostream>
#include <vector>

#include <euler.h>

int main(void) {
	unsigned N = 10000;

	std::vector<int> sumOfDivisors(N+1,1);
	std::vector<bool> amicable(N+1, false);

	for( int num = 1; num < N; ++num ) {
		for( int div = 2; div*div<=num; ++div ) {
			if( num%div == 0 ) {
				sumOfDivisors.at(num) += div;
				if( div*div != num ) {
					sumOfDivisors.at(num) += num/div;
				}
			}
		}
	}

	for( int a = 1; a < N; ++a ) {
		int b = sumOfDivisors.at(a);
		if( (a != b) & (b<N) ) {
			if(sumOfDivisors.at(b) == a) {
				std::cout << "d(" << a << ")=" << sumOfDivisors.at(a) << "\t"
						  << "d(" << b << ")=" << sumOfDivisors.at(b) << std::endl;
				amicable.at(a) = true;
				amicable.at(b) = true;
			}
		}
	}

	int sum = 0;

	for( int i = 1; i < N; ++i ) {
		if( amicable.at(i) ) {
			sum += i;
		}
	}

	std::cout << sum << std::endl;
	return 0;
}

#include <iostream>

#include <euler.h>

int main(void) {
	const uint64_t total_change = 200; // pence
	const std::vector<uint64_t> coins = {1, 2, 5, 10, 20, 50, 100, 200}; // Value in pence
	std::vector<uint64_t> ways(total_change+1, 0);
	ways.at(0) = 1; // Exactly one way to give zero change

	for( auto coin : coins ) {
		for( uint64_t change = coin; change <= total_change; ++change ) {
			ways.at(change) += ways.at(change - coin);
		}
	}
	
	std::cout << ways.at(total_change) << std::endl;
	return 0;
}



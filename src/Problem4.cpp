#include <iostream>

int reverseNumber(int number) {
	int reversed = 0;

	while( number>0 ) {
		reversed = reversed*10 + number%10;
		number /= 10;
	}

	return reversed;
}

bool isPalindrome(int number)
{
	if( number == reverseNumber(number) ) {
		return true;
	} else {
		return false;
	}
}

int main(void) {
	int largestPalindrome = 0;
	for( int i = 999; i >= 100; i-- ) {
		for( int j = i; j >= 100; j-- ) {
			int number = i*j;
			if( number > largestPalindrome ) {
				if( isPalindrome(number) ) {
					largestPalindrome = number;
				}
			}
		}
	}


	std::cout << largestPalindrome << std::endl;
	return 0;
}

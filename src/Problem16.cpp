#include <iostream>
#include <cstdint>
#include <cmath>

#include <euler.h>

int main(void) {
	int number = 2;
	int exponent = 1000;
	unsigned numDigits = std::ceil(exponent*std::log10(number));
	
	std::vector<int64_t> product;
	product.resize(numDigits,0);
	product.back() = number;
	for( int i = 1; i < exponent; i++ ) {
		int carry = 0;
		for( auto it = product.rbegin(); it != product.rend(); ++it ) {
			*it   = (*it)*number + carry;
			carry = (*it)/10;
			*it   = (*it)%10;
		} 
	}

	std::cout << "digitsum(";

	int sum = 0;

	for( auto digit : product ) {
		std::cout << digit;
		sum += digit;
	}

	std::cout << ") = " << sum << std::endl;
	return 0;
}

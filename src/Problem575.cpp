#include <iostream>
#include <iomanip>
#include <numeric>

#include <euler.h>

int main(void) {
	const int64_t grid_size = 1000;

	// The rooms can be put into three categories: corners, sides and middle.
	uint64_t corner_rooms = 4;
	uint64_t edge_rooms = 4*(grid_size-2);
	uint64_t center_rooms = (grid_size-2)*(grid_size-2);

	// For the first scenario there are three ways to get into a corner: moving
	// from the adjacent edge rooms or staying in the corner. Similarly there is
	// four ways to get to a edge room and five to a center room. 
	uint64_t actions1_corner = 3; 
	uint64_t actions1_edge = 4; 
	uint64_t actions1_center = 5; 

	// In the second case the probability of remaining in a room is equal for all
	// rooms and this action can therefore be ignored.
	uint64_t actions2_corner = 2; 
	uint64_t actions2_edge = 3; 
	uint64_t actions2_center = 4;

	// Sum the total number of actions for both scenarios.
	uint64_t actions1 = corner_rooms*actions1_corner + edge_rooms*actions1_edge + center_rooms*actions1_center;
	uint64_t actions2 = corner_rooms*actions2_corner + edge_rooms*actions2_edge + center_rooms*actions2_center;

	// Loop through all square numbered rooms.
	double p_square_numbered_room = 0.0;
	for( uint64_t i = 1; i*i <= grid_size*grid_size; ++i ) {
		uint64_t room_number = i*i;

		// Is room on an edge?
		bool on_left_edge = (room_number%grid_size == 1);
		bool on_right_edge = (room_number%grid_size == 0);
		bool on_top_edge = (room_number/grid_size == 0);
		bool on_bottom_edge = (room_number/grid_size == grid_size);

		// Add to the total probability for that Leonhard is in a square
		// numbered room depending on its type.
		// Corner:
		if( (on_left_edge || on_right_edge) && (on_top_edge || on_bottom_edge) ){
			p_square_numbered_room += ( (double)actions1_corner/actions1 + (double)actions2_corner/actions2 )/2.0;
		// Edge:
		} else if ( on_left_edge || on_right_edge || on_top_edge || on_bottom_edge ) {
			p_square_numbered_room += ( (double)actions1_edge/actions1 + (double)actions2_edge/actions2 )/2.0;
		// Center:
		} else {
			p_square_numbered_room += ( (double)actions1_center/actions1 + (double)actions2_center/actions2 )/2.0;
		}
	}

	std::cout << std::fixed << std::setprecision(12) << p_square_numbered_room << std::endl; 

	return 0;
}

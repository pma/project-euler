#include <iostream>
#include <cmath>

int main(void) {
	int N = 100;

	int answer = pow(N*(N+1)/2,2) - (2*pow(N,3) + 3*pow(N,2) + N)/6;

	std::cout << answer << std::endl;
	return 0;
}

#include <iostream>

#include <euler.h>

int main(void) {
	std::vector<uint64_t> factorial_digits;
	factorial_digits.push_back(1);
	for( uintptr_t i = 1; i <= 9; ++i ) {
		factorial_digits.push_back(factorial_digits.back()*i);
	}

	uint64_t upper_bound_digits = 9;
	uint64_t upper_bound = factorial_digits.at(9);
	while( upper_bound_digits < upper_bound ) {
		upper_bound_digits = upper_bound_digits*10 + 9;
		upper_bound += factorial_digits.at(9);
	}
	std::cout << "Upper bound = " << upper_bound << std::endl;

	uint64_t sum_of_digit_factorial_numbers = 0;
	for( uint64_t number = 10; number <= upper_bound; ++number ) {
		uint64_t temp = number;
		std::vector<uint64_t> digits;
		while( temp != 0 ) {
			digits.push_back(temp%10);
			temp/=10;
		}

		uint64_t digit_factorial_sum = 0;
		for( auto digit : digits ) {
			digit_factorial_sum += factorial_digits.at(digit);
		}

		if( number == digit_factorial_sum ) {
			sum_of_digit_factorial_numbers += number;
			
			std::cout << digits.back() << "!";
			for( auto dit = digits.rbegin() + 1; dit != digits.rend(); ++dit ) {
				std::cout << " + " << (*dit) << "!";
			}
			std::cout << " = ";
			std::cout << factorial_digits.at(digits.back());
			for( auto dit = digits.rbegin() + 1; dit != digits.rend(); ++dit ) {
				std::cout << " + " << factorial_digits.at(*dit);
			}
			std::cout << " = " << number << std::endl;
		}
	}

	std::cout << "Sum of all numbers with digit factorials = " << sum_of_digit_factorial_numbers << std::endl;
	return 0;
}

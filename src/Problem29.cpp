#include <algorithm>
#include <cmath>
#include <iostream>
#include <set>

#include <euler.h>

bool operator<(std::vector<uint64_t>& lhs, std::vector<uint64_t>& rhs) {
	if( lhs.size() < rhs.size() ) {
		return true;
	} else if( lhs.size() > rhs.size() ) {
		return false;
	} else {
		return std::lexicographical_compare( lhs.rbegin(), lhs.rend(), rhs.rbegin(), rhs.rend() );
	}
}

int main(void) {
	const unsigned upper = 100;
	std::set<std::vector<uint64_t>> numbers;
	uint64_t base = 1000000000;

	unsigned numUniqueTerms = (upper-1)*(upper-1);
	for( uint64_t a = 2; a <= upper; ++a ) {
		std::vector<uint64_t> num;
		num.push_back(a);
		for( uint64_t b = 2; b <= upper; ++b ) {
			uint64_t carry = 0;
			for( auto it = num.begin(); it != num.end(); ++it ) {
				*it *= a;
				*it += carry;
				carry = *it/base;
				*it %= base;
			}
			while( carry != 0 ) {
				num.push_back(carry%base);
				carry /= base;
			}
			numbers.insert(num);
		}
	}

	std::cout << numbers.size() << std::endl;
	return 0;
}

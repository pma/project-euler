#include <cmath>
#include <iostream>

#include <euler.h>

int main(void) {
	uint64_t N = 1000000;
	std::set<uint64_t> primes = euler::find_primes(N);
	std::vector<bool> sieve(N,false);
	for( auto prime : primes ) {
		sieve.at(prime) = true;
	}
	uint64_t numCircularPrimes = 0;

	for( auto prime : primes) {
		uint32_t digits = std::ceil( std::log10( prime ));
		//std::cout << prime;
		if( digits == 1 ) {
			numCircularPrimes++;
		} else {
			uint64_t candidate = prime;
			uint64_t matches = 1;
			for( uint64_t i = 1; i < digits; ++i ) {	
				candidate *= 10;
				candidate += candidate/(uint64_t)std::pow(10,digits);
				candidate %= (uint64_t)std::pow(10,digits);
				//std::cout << " " << candidate;
				if( sieve.at(candidate) == true ) {	
					matches++;
				}
			}
			if( matches == digits ) {
				numCircularPrimes++;
			}
		}
		//std::cout << std::endl;
	}

	std::cout << numCircularPrimes <<  std::endl;
	return 0;
}

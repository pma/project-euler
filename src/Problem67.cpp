#include <iostream>
#include <fstream>

#include <euler.h>

int main(void) {
	std::ifstream pyramidFile;
	euler::Pyramid<int> pyramid;

	// Load pyramid from file
	pyramidFile.open("./data/p067_triangle.txt");
	if( !pyramidFile.is_open() ) {
		return -255;
	}
	pyramidFile >> pyramid;

	pyramid.max_sum_path();
	std::cout << "Problem 67 pyramid:" << std::endl
	          << pyramid
	          << "Maximum sum = " << pyramid.max_sum_path() << std::endl;

	return 0;
}

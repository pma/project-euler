#include <cmath>
#include <cstdint>
#include <iostream>
#include <limits>
#include <vector>

#include <euler.h>

int main(void) {
	unsigned N = 100;

	std::vector<uint64_t> words;
	words.push_back(1);
	unsigned numDigits = std::floor(std::log10(std::sqrt(std::numeric_limits<uint64_t>::max())));
	uint64_t base = std::pow(10,numDigits);

	for( unsigned n = 1; n<=N; ++n ) {

		uint64_t carry = 0;

		for( auto dit = words.begin(); dit != words.end(); ++dit ) {
			*dit *= n;
			*dit += carry;
			carry = *dit/base;
			*dit %= base;
		}

		if( carry != 0 ) {
			words.push_back(carry);
		}
	}

	std::cout << "Digitsum( ";

	while( words.back() == 0 ) {
		words.pop_back();
	}

	std::vector<unsigned> digits;

	for( auto word : words ) {
		auto mask = base;
		for( int d = numDigits-1; d >= 0; --d ) {
			mask /= 10;
			auto digit = (word/mask)%10;
			if( !digits.empty() || digit != 0 ) {
				digits.push_back(digit);
			}
		}
	}

	unsigned digitSum = 0;
	for( auto d : digits ) {
		std::cout << d;
		digitSum += d;
	}

	std::cout << " ) = " << digitSum << std::endl;
	return 0;
}

#include <iostream>

#include <euler.h>

int main(void) {
	uint64_t N = 10000000;

	std::vector<uint64_t> arrive(N,0);
	arrive.at(1) = 1;
	arrive.at(89) = 89;

	std::vector<uint64_t> squares;
	for( uint64_t i = 0; i <= 9; ++i ) {
		squares.push_back(i*i);
	}

	uint64_t numArriveAt89 = 0;

	std::vector<uint64_t> chain;
	for( uint64_t n = 1; n<N; ++n ) {
		if ( arrive.at(n) == 0 ) {
			uint64_t i = n;
			while( true ) {
				if( i < N ) {
					if( arrive.at(i) == 89 || arrive.at(i) == 1 ) {
						break;
					} else {
						chain.push_back(i);
					}
				}

				uint64_t next = 0;
				while( i != 0 ) {
					uint64_t digit = i%10;
					i/=10;
					next += squares.at(digit);
				}

				i = next;
			}

			for( auto link : chain ) {
				arrive.at(link) = arrive.at(i);
			}
			chain.clear();
		}
	}

	for( auto arrives : arrive ) {
		if( arrives == 89 ) {
			numArriveAt89++;
		}
	}
		
	std::cout << numArriveAt89 << std::endl;
	return 0;
}

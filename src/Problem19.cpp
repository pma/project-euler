#include <iostream>

#include <euler.h>

int main(void) {
	unsigned numSundayFirst = 0;
	unsigned day = 0;
	unsigned daysMonth[12][2] = {{31,31},
	                             {28,29},
	                             {31,31},
	                             {30,30},
	                             {31,31},
	                             {30,30},
	                             {31,31},
	                             {31,31},
	                             {30,30},
	                             {31,31},
	                             {30,30},
	                             {31,31}};

	for( unsigned year = 1900; year <= 2000; year++ ) {
		bool leapYear;
		if( (year%4 == 0) && ((year%100 != 0) || (year%400 == 0)) ) {
			leapYear = true;
		} else {
			leapYear = false;
		}

		for( unsigned month = 0; month < 12; month++ ) {
			unsigned weekDay = day%7;
			if( (weekDay == 6) && (year >= 1901) ) {
				numSundayFirst++;
			}
			day += daysMonth[month][leapYear?1:0];
		}
	}

	std::cout << numSundayFirst++ << std::endl;
	return 0;
}

#include <cstdint>
#include <iostream>

#include <euler.h>

int main(void) {
	euler::Fraction product(1);

	const std::vector<std::vector<uint_fast8_t>> combinations = {{0,0},{0,1},{1,0},{1,1}};
	for( uint_fast16_t denominator = 10; denominator < 100; ++denominator ) {
		for( uint_fast16_t numerator = 10; numerator < denominator; ++numerator ) {
			std::vector<uint_fast8_t> den_digits = { denominator/10, denominator%10 };
			std::vector<uint_fast8_t> num_digits = { numerator/10, numerator%10 };

			for( auto combination : combinations ) {
				if( ( num_digits.at(combination.at(0)) == den_digits.at(combination.at(1)) ) && ( num_digits.at(combination.at(0)) != 0 ) && ( den_digits.at(1-combination.at(1)) != 0 ) ) {
					euler::Fraction fraction(numerator,denominator);
					euler::Fraction dumb_simplification(num_digits.at(1-combination.at(0)),den_digits.at(1-combination.at(1)));
					if( fraction == dumb_simplification ) {
						std::cout << fraction << " = " << dumb_simplification << std::endl;
						product *= dumb_simplification;
					}
				}
			}
		}
	}
	std::cout << product.simplify().denominator << std::endl;
	return 0;
}

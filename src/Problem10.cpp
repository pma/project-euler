#include <iostream>

#include <euler/prime.h>

int main(void) {
	auto primes = euler::find_primes(2e6);

	long sum = 0;
	for( auto prime : primes ) {
		sum += prime;
	}
	std::cout << sum << std::endl;
	
	return 0;
}

#include <iostream>

#include <euler/fibonacci.h>

int main(void) {

	auto numbers = euler::fibonacci_sequence(0, 4e6);

	int sum = 0;

	for( auto number : numbers ) {
		if( number%2 == 0 ) {
			sum += number;
		}
	}

	std::cout << sum << std::endl;
	return 0;
}

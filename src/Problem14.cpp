#include <iostream>

#include <euler.h>

int main(void) {
	unsigned int N = 1000000;

	unsigned int longest = 0;
	unsigned int maxLength = 0;

	std::vector<int> lengths;
	lengths.resize(N, 0);
	lengths.at(1) = 1;
	
	for( unsigned int i = 2; i < N; i++ ) {
		unsigned int n = i;

		do {
			lengths.at(i)++;
			if( n%2 == 0 ) {
				n = n/2;
			} else {
				n = 3*n + 1;
			}
		} while( n > i );
		
		lengths.at(i) += lengths.at(n);

		if( lengths.at(i) > maxLength ) {
			maxLength = lengths.at(i);
			longest = i;
		}
	}

	std::cout << "Starting at " << longest << " creates a chain of " << maxLength << std::endl;
	return 0;
}

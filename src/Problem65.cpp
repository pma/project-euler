#include <iostream>
#include <vector>

#include <euler/fraction.h>

int main(void) {
	uint64_t N = 10;

	std::cout << "e = [2; ";

	std::vector<int64_t> divisors;
	int64_t n = 0;
	for( uint64_t i = 1; i < N; ++i ) {
		if( (i+1)%3 == 0 ) {
			n += 2;
			divisors.push_back(n);
		} else {
			divisors.push_back(1);
		}
		std::cout << divisors.back() << ",";
	}
	std::cout << "\b]" << std::endl;

	euler::Fraction fraction(1,divisors.at(0));

	for( auto it = divisors.rbegin()+1; it < divisors.rend(); ++it ) {
		fraction = (int64_t) 1 / ( *it + fraction );
		fraction.simplify();
	}
	
	euler::Fraction e = (int64_t) 2 + fraction;
	e.simplify();
	std::cout << "  = " << e << " = " << (double)e << std::endl;

	return 0;
}

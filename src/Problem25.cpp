#include <cmath>
#include <iostream>
#include <vector>
#include <iomanip>

#include <euler.h>

int main(void) {
	std::vector<uint64_t> words;
	std::vector<uint64_t> wordsNext;
	std::vector<uint64_t> wordsPrevious;
	uint64_t digitsInWord = 10;
	uint64_t base = std::pow(10,digitsInWord );
	words.push_back(1);
	uint64_t digits = 1;
	uint64_t index = 1;

	while( digits < 1000 ) {
		index++;
		uint64_t carry = 0;
		wordsPrevious.resize(words.size(),0);

		auto wp = wordsPrevious.begin();
		for( auto w = words.begin(); w != words.end(); ++w, ++wp ) {
			wordsNext.push_back( *w + *wp + carry );
			carry = wordsNext.back()/base;
			wordsNext.back()%=base;
		}

		if( carry != 0 ) {
			wordsNext.push_back(carry);
		}

		digits = digitsInWord*(wordsNext.size()-1);
		uint64_t remainder = wordsNext.back();
		while( remainder != 0 ) {
			remainder/=10;
			++digits;
		}

		wordsPrevious.swap(words);
		words.swap(wordsNext);
		wordsNext.clear();
	}

	for( auto word : words ) {
		if( word == words.back() ) {
			std::cout << word;
		} else {
			std::cout << std::setw(10) << std::setfill('0') << word;
		}
	}
	std::cout << std::endl;

	std::cout << index << std::endl;
	return 0;
}

#include <iostream>
#include <set>

#include <euler.h>

int main(void) {
	const int A = 1000;
	const int B = 1001;

	const std::set<uint64_t> primes = euler::find_primes(10000000); 

	unsigned nMax = 0;
	int aMax;
	int bMax;
	for( auto bit = primes.begin(); bit != primes.upper_bound(B); ++bit ) {
		int b = *bit;
		for( int a = -A; a < A; ++a ) {
			unsigned n = 0;
			while( primes.count(n*n + a*n + b) ) {
				++n;
			}
			if( n >= nMax ) {
				nMax = n;
				aMax = a;
				bMax = b;
			}
		}
	}
	std::cout << "n= " << nMax << " a=" << aMax << " b=" << bMax << " a*b=" << aMax*bMax << std::endl;
	return 0;
}

#include <bitset>
#include <iomanip>
#include <iostream>

#include <euler.h>

int main(void) {
	const uint64_t upper_bound = 1000000;
	uint64_t sum_of_base10_and_base2_palindormes = 0;

	for( auto odd_length : {true, false} ) {
		uint64_t palindrome_base2;
		for( uint64_t i = 1; (palindrome_base2 = euler::makePalindromeBase2(i,odd_length)) < upper_bound; ++i ) {
			if( euler::isPalindrome( palindrome_base2, 10 ) ) { 
				sum_of_base10_and_base2_palindormes += palindrome_base2;
				std::cout << std::setw(10) << palindrome_base2 << " " << std::bitset<32>(palindrome_base2) << std::endl;
			}
		}
	}

	std::cout << "Sum of number that are palindromes in both base 10 and 2 under " << upper_bound << ": " << sum_of_base10_and_base2_palindormes << std::endl;
	return 0;
}

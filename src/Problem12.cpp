#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdint>

#include <euler/prime.h>

int main(void) {
	int minNumDivisors = 500;
	int maxNumDivisors = 0;
	int numDivisors;
	uint32_t number = 1;
	uint32_t n = 1;
	uint32_t maxDivisor;

	int maxPrime = floor(sqrt(UINT32_MAX));
	std::cout << "Generating prime table up to " << maxPrime << "..." << std::endl;
	auto primes = euler::find_primes(maxPrime,0);
	
	std::cout << "Searching for the smallest triangle number with more than " << minNumDivisors << " divisors..." << std::endl;

	while( numDivisors <= minNumDivisors ) {
		// Increment to next triangle number.
		n++;
		if( UINT32_MAX-number < n ) {
			std::cout << std::endl << "Overflow!" << std::endl;
			return -1;
		}
		number += n;

		// Prime factor the number, i.e write it in the form
		// N = a1*p1 + a2*p2 + ... + an*pn, where pn are distinct primes and
		// an are integer coefficients. The number of divisors are then given by
		// D(N) = (a1+1)*(a2+1)*...*(an+1) + 1
		uint32_t sqrtNumber = floor(sqrt(number));
		uint32_t testNumber = number;
		numDivisors = 1; 
		for( auto prime : primes ) {
			int an = 0;
			if( prime > sqrt(number) ) {
				break;
			}

			while( testNumber%prime == 0 ) {	
				testNumber /= prime;
				an++;
			}
			numDivisors *= an + 1;
		}

		// Add 1 as when count 1 as a divisor.
		numDivisors += 1;
	}

	std::cout << number << " has " << numDivisors << " divisors." << std::endl;

	return 0;
}
